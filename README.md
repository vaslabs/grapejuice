# Grapejuice

⚠️ Roblox is going through some drastic changes recently.

Roblox Studio is using some newer Windows API calls which aren't supported by Wine yet, which causes launch failures. On the other hand, the Player is transitioning to 64-bit only mode and new anti-cheat features. Grapejuice still works and these changes are only being rolled out to certain people. You can still try it out, but your mileage may vary.

The Studio issue is going to require some patches to Wine, which may take a while to be fully upstreamed. Changes to the Player are in progress and not definitive yet. You might get a 'Wine is not supported' error message. A temporary workaround is to enter `roblox-player:1+launchmode:app+channel:zlive` in your browser URL bar.

As it stands, Roblox Studio is considered broken. However, the Player still works for the vast majority of people.

If you would like to contribute to these issues with other people in the community, you are free to join the Grapejuice Discord server: https://discord.gg/RjtbVPZdGm

---

These days Roblox works quite well with Wine, though there are some minor issues running it. Grapejuice fills in the
gaps around these issues, so you can experience Roblox to its fullest on your favourite Linux distribution.

The primary gap-filler feature is the fact that Wine by default creates no protocol handlers, which is how Roblox
functions at its core. Without protocol handling, you won't be able to launch Roblox Studio and Experiences from the
website!

Note that Grapejuice is unofficial software. This is not officially supported by Roblox.

## Installing Grapejuice

The installation guides can be found in the [documentation](https://brinkervii.gitlab.io/grapejuice/docs/).

## Troubleshooting

Are you experiencing some trouble running Roblox Studio with Grapejuice? Please check out
the [troubleshooting guide](https://brinkervii.gitlab.io/grapejuice/docs/Troubleshooting).

## Features

- Sets up a Wine prefix automatically
- Edit Roblox experiences from the website
- Enjoy Roblox experiences by launching them from the website
- FFlag editor for tweaking Roblox's behaviour
